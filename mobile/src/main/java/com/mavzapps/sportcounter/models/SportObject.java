package com.mavzapps.sportcounter.models;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by MariaVirginia on 22.11.2015.
 */
public class SportObject {

    private String name;
    private int id;
    private Drawable drawable;
    private Intent intent;

    public void setName(String name)
    {
        this.name = name;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setDrawable(Drawable drawable)
    {
        this.drawable = drawable;
    }

    public void setIntent(Intent intent){
        this.intent = intent;
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }

    public Drawable getDrawable()
    {
        return drawable;
    }

    public Intent getIntent(){
        return intent;
    }
}
