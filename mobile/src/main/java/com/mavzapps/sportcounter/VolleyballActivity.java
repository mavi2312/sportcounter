package com.mavzapps.sportcounter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class VolleyballActivity extends AppCompatActivity {

    ImageView img;
    String msg="Mensaje";
    //private android.widget.RelativeLayout.LayoutParams layoutParams;
    int windowwidth;
    int windowheight;
    private View team1, team2;
    int  countTeam1, countTeam2;
    private TextView labelTeam1, labelTeam2, labelTimer, labelWinner;
    private LinearLayout finishLayout;
    private Button yesBtn, noBtn;
    private boolean gameOver;
    //private RelativeLayout.LayoutParams layoutParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volleyball);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        gameOver = false;
        countTeam1=0;
        countTeam2=0;

        labelTeam1 = (TextView)findViewById(R.id.team1Points);
        labelTeam2 = (TextView)findViewById(R.id.team2Points);
        labelTimer = (TextView)findViewById(R.id.timer);
        labelWinner = (TextView)findViewById(R.id.winner);

        finishLayout = (LinearLayout)findViewById(R.id.finishLayout);
        yesBtn = (Button)findViewById(R.id.yesBtn);
        noBtn = (Button)findViewById(R.id.noBtn);

        img=(ImageView)findViewById(R.id.soccer_ball);
        team1=(View)findViewById(R.id.team1);
        team2=(View)findViewById(R.id.team2);
        img.setTag("stuff");

        windowwidth = getWindowManager().getDefaultDisplay().getWidth();
        windowheight = getWindowManager().getDefaultDisplay().getHeight();

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                labelWinner.setText("");
                labelTeam1.setText("0");
                labelTeam2.setText("0");
                labelTimer.setText("");
                countTeam1 = 0;
                countTeam2 = 0;
                gameOver = false;
                finishLayout.setVisibility(View.GONE);

            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VolleyballActivity.this.finish();
            }
        });

        img.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) img.getLayoutParams();
                layoutParams.removeRule(RelativeLayout.CENTER_IN_PARENT);
                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int x_cord = (int)event.getRawX();
                        int y_cord = (int)event.getRawY();

                        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                            if(x_cord>1060){x_cord=1060;}
                            if(y_cord>605){y_cord=605;}
                        }else{
                            if(x_cord>605){x_cord=605;}
                            if(y_cord>1060){y_cord=1060;}
                        }

                        if(x_cord<147){x_cord=147;}
                        if(y_cord<301){y_cord=301;}

                        layoutParams.leftMargin = x_cord-147;
                        layoutParams.topMargin = y_cord-301;

                        img.setLayoutParams(layoutParams);
                        Log.d(msg, "ACTION.MOVE coord X: "+String.valueOf(x_cord)+"coord Y: "+String.valueOf(y_cord));
                        /*layoutParams = (RelativeLayout.LayoutParams)img.getLayoutParams();
                        int x_cord = (int) event.getX();
                        int y_cord = (int) event.getY();
                        layoutParams.leftMargin = x_cord;
                        layoutParams.topMargin = y_cord;
                        img.setLayoutParams(layoutParams);*/
                        break;
                    case MotionEvent.ACTION_UP:
                        if(isViewOverlapping(img,team1)){
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                            img.setLayoutParams(layoutParams);
                            if(!gameOver){
                                countTeam1++;
                                labelTeam1.setText(String.valueOf(countTeam1));
                            }
                        }else if(isViewOverlapping(img,team2)){
                            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                            img.setLayoutParams(layoutParams);
                            if(!gameOver){
                                countTeam2++;
                                labelTeam2.setText(String.valueOf(countTeam2));
                            }
                        }

                        if((countTeam1 >= 25 || countTeam2 >= 25) && (Math.abs(countTeam1-countTeam2)>=2) ){
                            labelTimer.setText("The game has ended.");
                            finishLayout.setVisibility(View.VISIBLE);
                            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                                if(countTeam1<countTeam2)
                                    labelWinner.setText("The second team wins! " +
                                            "Want to start again?");
                                else if(countTeam1>countTeam2)
                                    labelWinner.setText("The first team wins! " +
                                            "Want to start again?");
                                else
                                    labelWinner.setText("It's a tie! " +
                                            "Want to start again?");
                            }else{
                                if(countTeam1<countTeam2)
                                    labelWinner.setText("The second team wins!\n" +
                                            "Want to start again?");
                                else if(countTeam1>countTeam2)
                                    labelWinner.setText("The first team wins!\n" +
                                            "Want to start again?");
                                else
                                    labelWinner.setText("It's a tie!\n" +
                                            "Want to start again?");
                            }
                        }
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private boolean isViewOverlapping(View firstView, View secondView) {

        final int[] location = new int[2];

        firstView.getLocationInWindow(location);
        Rect rect1 = new Rect(location[0], location[1],location[0] + firstView.getWidth(), location[1] + firstView.getHeight());

        secondView.getLocationInWindow(location);
        Rect rect2 = new Rect(location[0], location[1],location[0] + secondView.getWidth(), location[1] + secondView.getHeight());

        return rect1.intersect(rect2);
//        return  (rect1.contains(rect2)) || (rect2.contains(rect1));
    }






}
