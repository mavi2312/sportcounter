package com.mavzapps.sportcounter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mavzapps.sportcounter.models.SportObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class SelectSport extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_sport);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ArrayList<SportObject> sportObjectList = new ArrayList<SportObject>();
        SportObject volleyball = new SportObject();
        volleyball.setName("Volleyball");
        volleyball.setId(0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            volleyball.setDrawable(this.getResources().getDrawable(R.drawable.volleyball_ball,null));
        }else{
            volleyball.setDrawable(this.getResources().getDrawable(R.drawable.volleyball_ball));
        }
        Intent intent1 = new Intent(SelectSport.this,VolleyballActivity.class);
        volleyball.setIntent(intent1);
        sportObjectList.add(0,volleyball);

        SportObject soccer = new SportObject();
        soccer.setName("Soccer");
        soccer.setId(1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            soccer.setDrawable(this.getResources().getDrawable(R.drawable.soccer_ball,null));
        }else{
            soccer.setDrawable(this.getResources().getDrawable(R.drawable.soccer_ball));
        }
        Intent intent2 = new Intent(SelectSport.this,SoccerActivity.class);
        soccer.setIntent(intent2);
        sportObjectList.add(0,soccer);


        SportObject basketball = new SportObject();
        basketball.setName("Basketball");
        basketball.setId(1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            basketball.setDrawable(this.getResources().getDrawable(R.drawable.basketball_ball,null));
        }else{
            basketball.setDrawable(this.getResources().getDrawable(R.drawable.basketball_ball));
        }
        Intent intent3 = new Intent(SelectSport.this,BasketballActivity.class);
        basketball.setIntent(intent3);
        sportObjectList.add(2,basketball);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(this.getApplicationContext());
        rv.setLayoutManager(llm);

        RVAdapter adapter = new RVAdapter(sportObjectList);
        rv.setAdapter(adapter);

    }

    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.SportsViewHolder>{

        public class SportsViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView sportName;
            ImageView sportPhoto;

            SportsViewHolder(View itemView) {
                super(itemView);
                cv = (CardView)itemView.findViewById(R.id.cv);
                sportName = (TextView)itemView.findViewById(R.id.titleTextView);
                sportPhoto = (ImageView)itemView.findViewById(R.id.imageSportList);
            }
        }

        private ArrayList<SportObject> list;
        public RVAdapter(ArrayList<SportObject> list)
        {
            this.list = list;
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public SportsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sport_list_element, viewGroup, false);
            SportsViewHolder svh = new SportsViewHolder(v);
            return svh;
        }

        @Override
        public void onBindViewHolder(SportsViewHolder sportViewHolder, int i) {
            sportViewHolder.sportName.setText(list.get(i).getName());
            sportViewHolder.sportPhoto.setImageDrawable(list.get(i).getDrawable());
            sportViewHolder.cv.setTag(i);
            sportViewHolder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("Tag",String.valueOf((int)v.getTag())) ;
                   startActivity(list.get((int)v.getTag()).getIntent());
                }
            });
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }
    }

}
